﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerLight : MonoBehaviour {

	private Light li;
	// Use this for initialization
	void Start () {
		li = GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {
		li.intensity = 5*(1 - Mathf.Abs(Mathf.Cos(Time.time * Mathf.PI / 60)));
	}
}
