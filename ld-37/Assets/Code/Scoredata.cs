﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoredata : MonoBehaviour {

	public int codeScore;
	public int artScore;

	// Use this for initialization
	void Start () {
		GameObject.DontDestroyOnLoad (this.gameObject);
	}

	public void setScore(int code, int art){
		this.codeScore = code;
		this.artScore = art;
	}

}
