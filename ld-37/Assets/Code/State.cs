﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State { Idle, Sleeping, Meditating, Programming, Drawing, Eating }
