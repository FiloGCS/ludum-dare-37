﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	//Vibration
	private float intensity = 0f;
	private int frameCount = 0;
	private float freq = 3;

	void FixedUpdate () {
		frameCount++;
		if (intensity != 0 && frameCount > freq) {
			frameCount = 0;
			if (Random.Range (0, 10) < 3) {
				Vector3 vibration = new Vector3 
					(Random.Range(0f,intensity/3)-intensity/6,
					 Random.Range(0f,intensity)  -intensity/2,
					 Random.Range(0f,intensity)  -intensity/2);
				transform.localEulerAngles = vibration;
			}
		}
	}

	public void setVibration(float newIntensity, float newFreq){
		intensity = newIntensity;
		freq = newFreq;
	}

}
