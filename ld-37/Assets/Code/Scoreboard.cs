﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

	public TextMesh text1;
	public TextMesh text2;
	public TextMesh text3;
	public TextMesh text4;
	public TextMesh text5;
	public TextMesh text6;
	public TextMesh text7;
	private Scoredata data;
	private AudioSource audioSource;
	public Material highlightMat;
	private Material lastMaterial;
	private Renderer lastRenderer = null;

	void Start () {
		text1.text = "";
		text2.text = "";
		text3.text = "";
		text4.text = "";
		text5.text = "";
		text6.text = "";
		text7.text = "";
		audioSource = GetComponent<AudioSource> ();
		data = GameObject.Find ("record").GetComponent<Scoredata> ();
	}

	void Update () {

		//Mouse hovering detection:
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit; 
		if (Physics.Raycast (ray, out hit)) {
			if (gameObject != lastRenderer) {
				//flush last Renderer buffer
				if (lastRenderer != null) { 
					lastRenderer.material = lastMaterial;
				}
				//Highlight
				Renderer hitRenderer = hit.collider.gameObject.GetComponent<Renderer> ();
				lastRenderer = hitRenderer;
				lastMaterial = lastRenderer.material;
				hitRenderer.material = highlightMat;
			}
		} else {
			//flush last Renderer buffer
			if (lastRenderer != null) {
				lastRenderer.material = lastMaterial;
				lastRenderer = null;
			}
		}

		//Mouseclick detection:
		if (Input.GetMouseButtonDown (0) && hit.collider != null) {
			if (hit.collider.name == "retry") {
				GameObject.Destroy (data.gameObject);
				Application.LoadLevel (1);
			}
			if (hit.collider.name == "exit") {
				Application.Quit ();
			}
		}


		if (Time.timeSinceLevelLoad > 1 && text1.text == "") {
			text1.text = "Code Score:";
		}
		if (Time.timeSinceLevelLoad > 2 && text2.text == "") {
			text2.text = data.codeScore.ToString();
			audioSource.Play ();
		}
		if (Time.timeSinceLevelLoad > 3 && text3.text == "") {
			text3.text = "Art Score:";
		}
		if (Time.timeSinceLevelLoad > 4 && text4.text == "") {
			text4.text = data.artScore.ToString();
			audioSource.Play ();
		}
		if (Time.timeSinceLevelLoad > 5 && text5.text == "") {
			text5.text = "Total Score";
		}
		if (Time.timeSinceLevelLoad > 7 && text6.text == "") {
			int bias = (int)Mathf.Pow (Mathf.Abs (data.artScore - data.codeScore), 1.1f);
			text6.text = (data.artScore + data.codeScore - bias).ToString();
			audioSource.Play ();
			if (bias > Mathf.Min (data.artScore, data.codeScore)) {
				text7.text = "Tip: Balance is very important!";
			}
		}



	}
}
