﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	private float deadline = 120;
	private float ORDER_TIME = 10;

	//Inspector public variables
	public Material highlightMat;
	public Material foodOrderMat;
	public GameObject clock_go;
	public GameObject door_go;
	public GameObject pizzaBox_go;
	public TextMesh progProgress_ui;
	public TextMesh artProgress_ui;
	public TextMesh countDown_ui;
	public TextMesh foodIsComing_text;
	public ProgressBar motivationBar;
	public ProgressBar sleepBar;
	public ProgressBar productivityBar;
	public ProgressBar foodBar;
	public ProgressBar foodOrderBar;
	public Blinker motUp;
	public Blinker sleepUp;
	public ParticleSystem codeParticles;
	public ParticleSystem sleepyParticles;
	public ParticleSystem sadParticles;
	public ParticleSystem foodParticles;
	public Light light;
	public TextMesh working;

	//Interface and Highlighting
	private Renderer lastRenderer = null;
	private Renderer doorRenderer = null;
	private Material lastMaterial;
	private string debugString = "";

	//Posible player positions
	public Transform progPosition;
	public Transform idlePosition;
	public Transform meditPosition;
	public Transform drawingPosition;
	public Transform sleepPosition;
	public Transform foodPosition;

	//Player data
	public GameObject player;
	public Player playerVib;
	private float progStep        =  10;
	private float drawingStep     =  10;
	private float motivationStep  =  20;
	private float sleepStep       =  20;
	private float motivationDecay =   2;
	private float sleepDecay      =   2;
	private float foodDecay       =   2;
    private float motivation      = 100;
    private float sleep           = 100;
	private float food            = 100;
	private float productivity    =   1;

	//Food order
	private float waitingTime     =   2;
	private float waiting         =   0;
	private float orderTime       =   0;
	private float orderWaitingTime=   0;
	public Material doorMaterial;
	public AudioClip eatingClip;
	public AudioClip orderingClip;
	public AudioSource dinger;
	private bool doorFix;

	//Game data
	private float progScope       = 100;
	private float artScope        = 100;
	private float progProgress    =   0;
	private float artProgress     =   0;
	private State state = State.Sleeping;

	//Color
	public Color dayLight;
	public Color nightLight;

	//Bubbles
	public Transform progBubbleSpawn;
	public GameObject progBubble;
	public Transform artBubbleSpawn;
	public GameObject artBubble;
	public AudioClip progBubbleClip;
	public AudioClip artBubbleClip;

	//Audio
	private AudioSource audioSource;

	//Scene swap
	private Fadeout fadeout;
	public AudioClip finishClip;
	public Scoredata scoreData;

	void Awake(){
		Time.timeScale = 0;
	}

	void Start(){
		fadeout = GetComponent<Fadeout> ();
		doorRenderer = door_go.GetComponent<Renderer> ();
		doorMaterial = doorRenderer.material;
		sleepyParticles.gameObject.SetActive (false);
		sadParticles.gameObject.SetActive (false);
		foodParticles.gameObject.SetActive (false);
		pizzaBox_go.SetActive (false);
		movePlayer ();
		audioSource = GetComponent<AudioSource> ();
		audioSource.volume = 0.5f;
	}

	void Update () {

		//light
		//light.intensity = Mathf.Cos(Time.timeSinceLevelLoad*Mathf.PI/30);
		light.color = Mathf.Abs(Mathf.Cos (Time.timeSinceLevelLoad * Mathf.PI / 60)) * dayLight + (1 - Mathf.Abs(Mathf.Cos(Time.timeSinceLevelLoad * Mathf.PI / 60))) * nightLight;

		//waiting detection
		if (waiting != 0 && Time.timeSinceLevelLoad - waiting >= waitingTime) {
			pizzaBox_go.SetActive (false);
			movePlayer ();
			waiting = 0;
		}

		//Mouse hovering detection:
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit; 
		if (Physics.Raycast (ray, out hit) && waiting == 0) {
			if (gameObject != lastRenderer) {
				//flush last Renderer buffer
				if (lastRenderer != null) { 
					lastRenderer.material = lastMaterial;
				}
				//Highlight
				Renderer hitRenderer = hit.collider.gameObject.GetComponent<Renderer> ();
				lastRenderer = hitRenderer;
				lastMaterial = lastRenderer.material;
				hitRenderer.material = highlightMat;
			}
		} else {
			//flush last Renderer buffer
			if (lastRenderer != null) {
				lastRenderer.material = lastMaterial;
				lastRenderer = null;
			}
		}

		if (doorFix) { //Spaghetti.cs
			deactivateDoor ();
			doorFix = false;
		}


		//Mouseclick detection:
		if (Input.GetMouseButtonDown (0) && hit.collider != null && waiting == 0) {
			Time.timeScale = 1;
			working.text = "";
			if (hit.collider.name == "computer") {
				flushParticles();
				state = State.Programming;
				movePlayer ();
			}
			if (hit.collider.name == "tablet") {
				flushParticles();
				state = State.Drawing;
				movePlayer ();
				playerVib.setVibration (10,3);
			}
			if (hit.collider.name == "window") {
				flushParticles();
				state = State.Meditating;
				movePlayer ();
				playerVib.setVibration (5,3);
			}
			if (hit.collider.name == "bed") {
				flushParticles();
				state = State.Sleeping;
				movePlayer ();
				playerVib.setVibration (10,20);
			}
			if (hit.collider.name == "phone" && orderTime == 0) {
				audioSource.clip = orderingClip;
				audioSource.Play();
				orderTime = Time.timeSinceLevelLoad;
				player.transform.SetParent (foodPosition, false);
				waiting = Time.timeSinceLevelLoad;
				waitingTime = 1; //ORDERING FOOD TIMER
				foodIsComing_text.text = "Food is coming!";
				playerVib.setVibration (5,3);
			}
			if (hit.collider.name == "door") {
				audioSource.clip = eatingClip;
				audioSource.Play();
				foodIsComing_text.text = "";
				orderWaitingTime = 0;
				foodOrderBar.current = 0;
				deactivateDoor();
				doorFix = true;
				player.transform.SetParent (foodPosition, false);
				waiting = Time.timeSinceLevelLoad;
				waitingTime = 2; //EATING FOOD TIMER
				food = 100;
				pizzaBox_go.SetActive (true);
				playerVib.setVibration (5,3);
			}
		}
	}
	void FixedUpdate(){

		if (deadline < Time.timeSinceLevelLoad && audioSource.clip != finishClip) {
			audioSource.clip = finishClip;
			audioSource.Play ();
			fadeout.fade();
		}
		if (Time.timeSinceLevelLoad >= deadline + 2) {
			scoreData.setScore ((int)progProgress,(int)artProgress);
			Application.LoadLevel (2);
		}

		//State-independent---------------------------------
		//food
		if (food > 0) {
			food -= foodDecay * Time.fixedDeltaTime;
			if (food <= 33)
				foodParticles.gameObject.SetActive(true);
		}
		else
			food = 0;
		//motivation
		motUp.  gameObject.SetActive(false);
		if (motivation > 0) {
			motivation -= motivationDecay * Time.fixedDeltaTime;
			if (motivation <= 33)
				sadParticles.gameObject.SetActive(true);
		}
		else
			motivation = 0;
		//sleep
		sleepUp.gameObject.SetActive(false);
		if (sleep > 0) {
			sleep -= sleepDecay * Time.fixedDeltaTime;
			if (sleep <= 33)
				sleepyParticles.gameObject.SetActive(true);
		}
		else
			sleep = 0;
		//productivity
		productivity = ((motivation + sleep) / 200) * Mathf.Sqrt(food/100);

		//State-driven+++++++++++++++++++++++++++++++++++++++
		int buffer = 0;
		switch (state) {
		//Idle
		case State.Idle:
			break;
		//Sleeping
		case State.Sleeping: 
			if (sleep <= 100) {
				sleepUp.gameObject.SetActive(true);
				sleepUp.setFreq ((100 - sleep)*0.075f);
				sleep += sleepStep * (100-sleep)/50 * Time.fixedDeltaTime;
				if (sleep > 33) {
					sleepyParticles.gameObject.SetActive (false);
				}
					
			} else {sleep = 100;}
			break;
		//Meditating
		case State.Meditating: 
			if (motivation <= 100) {
				motUp.gameObject.SetActive(true);
				motUp.setFreq ((100 - motivation)*0.075f);
				motivation += motivationStep * (100-motivation)/50 * Time.fixedDeltaTime;
				if (motivation > 33) {
					sadParticles.gameObject.SetActive (false);
				}
			} else {motivation = 100;}
			break;
		//Programming
		case State.Programming:
			if (waiting == 0 && Time.timeSinceLevelLoad < deadline ) {
				buffer = (int)progProgress;
				playerVib.setVibration (10, 1 / productivity);
				progProgress += progStep * productivity * Time.fixedDeltaTime + 0.0003f * progProgress;
				buffer = ((int)progProgress) - buffer;
				for (int i = 0; i < buffer; i++) {
					//Play programming clip
					audioSource.clip = progBubbleClip;
					audioSource.Play ();
					Vector3 spawnPosition = new Vector3 (progBubbleSpawn.position.x, progBubbleSpawn.position.y, progBubbleSpawn.position.z + 0.75f - Random.Range(0f,1.5f));
					Instantiate (progBubble, spawnPosition, Quaternion.identity);
				}
			}
			break;

		//Drawing
		case State.Drawing:
			if (waiting == 0 && Time.timeSinceLevelLoad < deadline) {
				buffer = (int)artProgress;
				playerVib.setVibration (10, 1/productivity);
				artProgress += drawingStep * productivity * Time.fixedDeltaTime + 0.0003f*artProgress;
				buffer = ((int)artProgress) - buffer;
				for (int i = 0; i < buffer; i++) {
					//Play art clip
					audioSource.clip = artBubbleClip;
					audioSource.Play ();
					Vector3 spawnPosition = new Vector3 (artBubbleSpawn.position.x, artBubbleSpawn.position.y, artBubbleSpawn.position.z + 0.75f - Random.Range(0f,1.5f));
					Instantiate (artBubble, spawnPosition, Quaternion.identity);
				}
			}
			break;
		}

		//UI Progress Bars
		motivationBar.current = motivation;
		sleepBar.current = sleep;
		productivityBar.current = productivity;
		foodBar.current = food;
		//food order
		if (orderTime != 0) {
			if ((Time.timeSinceLevelLoad - orderTime) / ORDER_TIME >= 1) {
				orderTime = 0;
				foodIsComing_text.text = "Answer the door!";
				orderWaitingTime = Time.timeSinceLevelLoad;
				activateDoor ();
			} else {
				foodOrderBar.current = (Time.timeSinceLevelLoad - orderTime) / ORDER_TIME;
			}
		}
		if (orderWaitingTime != 0) {
			if ((Time.timeSinceLevelLoad - orderWaitingTime) / ORDER_TIME * 3 >= 1) {
				orderWaitingTime = 0;
				deactivateDoor ();
			} else {
				foodOrderBar.current = (1-(Time.timeSinceLevelLoad - orderWaitingTime) / ORDER_TIME * 3);
			}
		}


		//UI texts Update
		progProgress_ui.text = ((int) progProgress).ToString();
		artProgress_ui.text = ((int) artProgress).ToString();
		countDown_ui.text = "Deadline: " + (int)(deadline - Time.timeSinceLevelLoad)*24/60/24 + " days and " + (int)(deadline - Time.timeSinceLevelLoad)*24/60%24 + " hours";
	}

	//GUI /////////////////////////////////////////////////////////////////
	void OnGUI(){
		/*
		GUI.Label (new Rect (10,  10, 200, 20), "Estado: "           + debugString);
		GUI.Label (new Rect (10,  50, 200, 20), "Programming done: " + progProgress / progScope * 100 + "%");
		GUI.Label (new Rect (10,  70, 200, 20), "Art done: "         + artProgress  / artScope  * 100 + "%");
		GUI.Label (new Rect (10, 100, 200, 20), "Motivation: "       + (int) motivation);
		GUI.Label (new Rect (10, 120, 200, 20), "Sleep: "            + (int) sleep);
		GUI.Label (new Rect (10, 140, 200, 20), "Productivity: "     + productivity);
		*/
	}

	void flushParticles(){
		codeParticles.gameObject.SetActive(false);
		sadParticles.gameObject.SetActive(false);
		foodParticles.gameObject.SetActive(false);
	}

	void activateDoor(){
		dinger.Play();
		Vector3 rotation = new Vector3 (0, 0, -80);
		door_go.transform.localEulerAngles = rotation;
		BoxCollider collider = door_go.GetComponent<BoxCollider> ();
		collider.enabled = true;
		doorRenderer.material = foodOrderMat;
	}
	void deactivateDoor(){
		doorRenderer.material = doorMaterial;
		Vector3 rotation = new Vector3 (0, 0, -90);
		door_go.transform.localEulerAngles = rotation;
		BoxCollider collider = door_go.GetComponent<BoxCollider> ();
		collider.enabled = false;
	}

	void movePlayer(){
		switch (state) {
		//Idle
		case State.Idle:
			break;
			//Sleeping
		case State.Sleeping: 
			player.transform.SetParent (sleepPosition, false);
			break;
			//Meditating
		case State.Meditating: 
			player.transform.SetParent (meditPosition, false);
			break;
			//Programming
		case State.Programming:
			player.transform.SetParent (progPosition, false);
			break;
			//Drawing
		case State.Drawing:
			player.transform.SetParent (drawingPosition, false);
			break;
		}
	}
}
