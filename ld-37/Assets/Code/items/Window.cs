﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour {

	public Color dayColor;
	public Color nightColor;
	private Renderer re;

	// Use this for initialization
	void Start () {
		re = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		re.materials[1].color = Mathf.Abs(Mathf.Cos (Time.time * Mathf.PI / 60)) * dayColor + (1 - Mathf.Abs(Mathf.Cos(Time.time * Mathf.PI / 60))) * nightColor;
	}
}
