﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour {

	public float freq = 10f;
	private Vector3 pivot;

	void Start(){
		pivot = transform.localPosition;
	}

	// Update is called once per frame
	void Update () {
		transform.localPosition = new Vector3 (pivot.x, pivot.y, pivot.z + Mathf.Sin(Time.time*freq) * 0.05f);
	}

	public void setFreq(float newfreq){
		this.freq = newfreq;
	}
}
