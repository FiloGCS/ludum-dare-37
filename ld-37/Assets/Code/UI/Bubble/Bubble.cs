﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour {

	public float yDespawn = 5.5f;
	public float speed = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (yDespawn > this.transform.position.y) {
			Vector3 position = this.transform.position;
			this.transform.position = new Vector3(position.x, position.y + speed*Time.deltaTime, position.z);
		} else {
			Destroy(this.gameObject);
		}
	}
}
