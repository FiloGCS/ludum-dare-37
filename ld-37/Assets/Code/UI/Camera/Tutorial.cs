﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {


	public TextMesh text1;
	public TextMesh text2;
	public TextMesh text3;
	public TextMesh text4;
	public TextMesh text5;
	public TextMesh text6;
	private AudioSource audioSource;

	void Awake(){
		text1.text = "";
		text2.text = "";
		text3.text = "";
		text4.text = "";
		text5.text = "";
		text6.text = "";
		audioSource = GetComponent<AudioSource> ();
	}

	void Update(){
		if (Time.time > 1 && text1.text == "") {
			text1.text = "Wake up...";
			audioSource.Play ();
		}
		if (Time.time > 3 && text2.text == "") {
			text2.text = "Ludum Dare 38 starts today.";
			audioSource.Play ();
		}
		if (Time.time > 6 && text3.text == "") {
			text3.text = "This ain't no weak-ass jam mate.";
			audioSource.Play ();
		}
		if (Time.time > 9 && text4.text == "") {
			text4.text = "This.";
			audioSource.Play ();
		}
		if (Time.time > 10 && text5.text == "") {
			text5.text = "Is.";
			audioSource.Play ();
		}
		if (Time.time > 11 && text6.text == "") {
			text6.text = "Compo.";
			audioSource.Play ();
		}
		if (Time.time > 13) {
			Application.LoadLevel (1);
		}
	}
}