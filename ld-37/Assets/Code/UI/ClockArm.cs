﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockArm : MonoBehaviour {

	public float speed = 20;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.eulerAngles = new Vector3 (Time.time*-speed, 90, 90);
	}
}
